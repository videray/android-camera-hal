#ifndef CAMERA_HAL_H_
#define CAMERA_HAL_H_

#include <cutils/bitops.h>
#include <hardware/hardware.h>
#include <hardware/camera_common.h>
#include <system/camera_vendor_tags.h>
#include "Camera.h"
#include "CameraHAL.h"

namespace camera_hal {
// CameraHAL contains all module state that isn't specific to an individual
// camera device.
class CameraHAL
{
public:
  CameraHAL();
  ~CameraHAL();

  int init();

  // Camera Module Interface (see <hardware/camera_common.h>)
  int getNumberOfCameras();
  int getCameraInfo( int camera_id, struct camera_info* info );
  int setCallbacks( const camera_module_callbacks_t* callbacks );
  void getVendorTagOps( vendor_tag_ops_t* ops );

  // Hardware Module Interface (see <hardware/hardware.h>)
  int open( const hw_module_t* mod, const char* name, hw_device_t** dev );

private:
  int mNumberOfCameras;
  const camera_module_callbacks_t* mCallbacks;
  // Array of camera devices, contains mNumberOfCameras device pointers
  Camera** mCameras;
};

} // namespace camera_hal

#endif // CAMERA_HAL_H_
