LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := camera.videray
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_VENDOR_MODULE := true
LOCAL_SHARED_LIBRARIES := \
  libcutils \
  libutils \
  libexif \
  libhardware \
  liblog \
  libsync \
  libcamera_metadata \
  libIllusion

LOCAL_STATIC_LIBRARIES := \
  libyuv_static

LOCAL_C_INCLUDES += 
LOCAL_SRC_FILES := \
  CameraHAL.cpp \
  Camera.cpp \
  Stream.cpp \
  Metadata.cpp \
  ExampleCamera.cpp \


include $(BUILD_SHARED_LIBRARY)