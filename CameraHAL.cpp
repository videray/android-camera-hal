
//#define LOG_NDEBUG 0
#define LOG_TAG "VideRayCameraHAL"

#include <cstdlib>

#include <log/log.h>
#define ATRACE_TAG (ATRACE_TAG_CAMERA | ATRACE_TAG_HAL)
#include <utils/Trace.h>

#include <hardware/camera_common.h>
#include <hardware/hardware.h>

#include "ExampleCamera.h"
#include "CameraHAL.h"

#define NUM_CAMERAS 1

namespace camera_hal {

CameraHAL::CameraHAL()
  : mNumberOfCameras( NUM_CAMERAS )
{
  ATRACE_CALL();

  mCameras = new Camera*[mNumberOfCameras];
  mCameras[0] = new ExampleCamera( 0 );
}

CameraHAL::~CameraHAL()
{
  ATRACE_CALL();
  for( int i = 0; i < mNumberOfCameras; i++ ) {
    delete mCameras[i];
  }

  delete[] mCameras;
  mCameras = nullptr;
}

int CameraHAL::init()
{
  ATRACE_CALL();
  return 0;
}

int CameraHAL::getNumberOfCameras()
{
  ATRACE_CALL();
  return mNumberOfCameras;
}

int CameraHAL::getCameraInfo( int camera_id, struct camera_info* info )
{
  ATRACE_CALL();
  if( ( camera_id < 0 ) || ( camera_id >= mNumberOfCameras ) ) {
    ALOGE( "Invalid camera id %d", camera_id );
    return -ENODEV;
  }
  return mCameras[camera_id]->getInfo( info );
}

int CameraHAL::setCallbacks( const camera_module_callbacks_t* callbacks )
{
  ATRACE_CALL();
  mCallbacks = callbacks;
  return 0;
}

void CameraHAL::getVendorTagOps( vendor_tag_ops_t* ops )
{
  ATRACE_CALL();
}

int CameraHAL::open( const hw_module_t* mod, const char* name, hw_device_t** dev )
{
  ATRACE_CALL();

  int id;
  //if (!android::base::ParseInt(name, &id, 0, getNumberOfCameras() - 1)) {
  //  return -EINVAL;
  //}

  return mCameras[id]->open(mod, dev);
}

} // namespace camera_hal

static camera_hal::CameraHAL gCameraHAL;

extern "C" {

  static int init()
  {
    return gCameraHAL.init();
  }

  static void get_vendor_tag_ops( vendor_tag_ops_t* ops )
  {
    gCameraHAL.getVendorTagOps( ops );
  }

  static int set_torch_mode( const char* camera_id, bool enabled )
  {
    return -ENOSYS;
  }

  static int get_number_of_cameras()
  {
    return NUM_CAMERAS;
  }

  static int get_camera_info( int id, struct camera_info* info )
  {
    return gCameraHAL.getCameraInfo( id, info );
  }

  static int set_callbacks( const camera_module_callbacks_t* callbacks )
  {
    return gCameraHAL.setCallbacks( callbacks );
  }

  static int open_dev( const hw_module_t* mod, const char* name, hw_device_t** dev )
  {
    return gCameraHAL.open( mod, name, dev );
  }

  static int open_legacy( const hw_module_t* module, const char* id, uint32_t halVersion, hw_device_t** device )
  {
    return -ENOSYS;
  }

  static hw_module_methods_t gCameraModuleMethods = {
    .open = open_dev
  };

  camera_module_t HAL_MODULE_INFO_SYM __attribute__( ( visibility( "default" ) ) ) = {
    .common = {
      .tag                = HARDWARE_MODULE_TAG,
      .module_api_version = CAMERA_MODULE_API_VERSION_2_4,
      .hal_api_version    = HARDWARE_HAL_API_VERSION,
      .id                 = CAMERA_HARDWARE_MODULE_ID,
      .name               = "VideRay Camera HAL",
      .author             = "VideRay Technologies",
      .methods            = &gCameraModuleMethods,
      .dso                = NULL,
      .reserved           = {0},
    },
    .get_number_of_cameras = get_number_of_cameras,
    .get_camera_info       = get_camera_info,
    .set_callbacks         = set_callbacks,
    .open_legacy           = open_legacy,
    .set_torch_mode        = set_torch_mode,
    .get_vendor_tag_ops    = get_vendor_tag_ops,
    .init                  = init,
    .reserved              = {0},
  };
} // extern "C"

