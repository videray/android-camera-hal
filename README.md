Android Camera HAL using V4L2

 * supports RAW sensor devices

To use this in AOSP add:

```
PRODUCT_PACKAGES += \
    camera.v4l2_simple \
    android.hardware.camera.provider@2.4-service

PRODUCT_PROPERTY_OVERRIDES += ro.hardware.camera=v4l2_simple
```

Modeled after hardware/libhardware/modules/camera/*